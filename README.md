# carmen parser

A simple script to parse CARMEN data files.

# Example usage

python carmen_parser.py --sensor-data <filename>.clf
