# Carmen parser
# author: P. Michael Furlong
# date: 2021-01-15
# 
# Usage: call carmen_parser on a file object that points to a carmen file (.clf)
# Example carmen files are found at:
# http://ais.informatik.uni-freiburg.de/slamevaluation/datasets.php

from argparse import ArgumentParser

import numpy as np

def parse_odom(tokens):
    t = float(tokens[-1])
    odom = {'x': float(tokens[0]),
            'y': float(tokens[1]),
            'theta': float(tokens[2]),
            'dtheta' : float(tokens[3]),
            'dr' : float(tokens[4]),
            'dx' : np.cos(float(tokens[2])) * float(tokens[4]),
            'dy' : np.sin(float(tokens[2])) * float(tokens[4]),
            'accel' : float(tokens[5]),
            }
    return (t, odom, None)

def parse_flaser(tokens):
    num_readings = int(tokens[0])
    readings = [float(x) for x in tokens[1:num_readings+1]]
    (t, odom, _) = parse_odom(tokens[num_readings + 1:])
    ranges = {'num_readings': num_readings, 'ranges': readings}

    return (t, odom, ranges) 

def parse_robotlaser1(tokens):
    raise NotImplementedError('Need to implement the Robot Laser 1 parser')

def tokens_to_ignore(line):
    tokens = line.split()
    return not ('#' in tokens[0] or 'PARAM' in tokens[0])

def carmen_parser(stream):
    line_parsers = {'ODOM': parse_odom, 'FLASER' : parse_flaser, 'ROBOTLASER1' : parse_robotlaser1}

    for line in filter(tokens_to_ignore, stream):
        tokens = line.split()
        if '#' in tokens[0] or 'PARAM' in tokens[0]:
            continue
        try:
            yield line_parsers[tokens[0]](tokens[1:])
        except KeyError as ke:
            print(f'Unrecognized token: {ke}')
            exit()



if __name__=='__main__':
    import matplotlib.pyplot as plt
    from matplotlib.animation import FuncAnimation

    parser = ArgumentParser()

    parser.add_argument('--sensor-data', dest='sensor_data', type=str)

    args = parser.parse_args()
   
    with open(args.sensor_data, 'r') as carmen_file:
        fig, ax = plt.subplots(subplot_kw={'projection':'polar'})
        tdata, rdata = np.linspace(0,np.pi,180), np.zeros((180,)) 
        ln, = ax.plot(tdata, rdata)

        def init():
            ax.set_xlim(0, 2.*np.pi)
            ax.set_ylim(0, 50)
            return ln,

        def update(frame):
            num_readings = frame[2]['num_readings']
            ranges = frame[2]['ranges']
            tdata = np.linspace(0, np.pi, num_readings)
            rdata = ranges
            ln.set_data(tdata, rdata)
            return ln,


        frames = filter(lambda obs: not obs[2] is None, carmen_parser(carmen_file))

        ani = FuncAnimation(fig, update, frames=frames, init_func=init, blit=True, interval=10)
        plt.show()
